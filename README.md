# Chrysalis Music
____
## Introducing the world's best music player. Now in beta.

Chrysalis Music is the first music player by Contol Alt Delete to work seamlessly with Google Play Music, Spotify, Last.fm, and SoundCloud. Built from the powerful and open-source [Harmony](https://www.getharmony.xyz) music player and the powerful interface of [Material Design Lite](https://www.getmdl.io), Chrysalis Music remains a powerful, yet user-friendly music player for macOS Sierra and Olivia (Project Starlight).

### Download now
**macOS Sierra**: [Download here](https://github.com/ubunturox104/chrysalis-music/releases)

**Olivia (Project Starlight)**: Chrysalis Music is built right into Olivia for the best music experience.

**Linux (via AppImage)**: Hold your horses! We're trying to convince the Queen to go in that territory.

### Known issues
Check the issues tab for all of that information. Kthxbye!

### Contact
Feel free to [contact us](mailto://officialcntlaltdel@gmail.com) via email, [visit our website](http://cntlaltdel.ga), or talk to us on [Facebook](http://www.facebook.com/cntlaltdel) to get in touch.